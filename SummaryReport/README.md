Summary Report: This play uses napalm_get_facts to obtain facts from two Cisco routers. A jinja2 template is used to output a summary report with uptime, available/used ram, OS and interfaces.
